package com.kazakimaru.ch04t4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.kazakimaru.ch04t4.databinding.FragmentBelajarSharedPrefBinding


class BelajarSharedPref : Fragment() {

    private var _binding: FragmentBelajarSharedPrefBinding? = null
    private val binding get() = _binding!!

    private lateinit var multiSharedPref: SharedPreferences

//    private val singleSharedPref = requireActivity().getPreferences(Context.MODE_PRIVATE)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentBelajarSharedPrefBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        multiSharedPref = requireContext().getSharedPreferences("this is id", Context.MODE_PRIVATE)

        btnSaveClicked()
        btnViewClicked()
        btnClearClicked()
    }

    private fun btnSaveClicked() {
        binding.btnSave.setOnClickListener {
            // get dari edittext
            val id = binding.etInputId.text.toString().toInt()
            val nama = binding.etInputName.text.toString()

            // Bikin editor
            val editor = multiSharedPref.edit()

            // Narok value kedalam shared pref
            editor.putInt("id", id)
            editor.putString("nama", nama)

            // nyimpan ke shared pref
            editor.apply()

            Toast.makeText(requireContext(), "Berhasil disimpan", Toast.LENGTH_SHORT).show()

        }
    }

    private fun btnViewClicked() {
        binding.apply {
            btnView.setOnClickListener {
                // dapetin value dari sharedpref
                val id = multiSharedPref.getInt("id", 12313)
                val nama = multiSharedPref.getString("nama", "default value")

//                if (nama.isNullOrEmpty()) {
//                    Toast.makeText(requireContext(), "Nama masih kosong", Toast.LENGTH_SHORT).show()
//                } else {
//                }

                // munculin value pada textview
                tvShowId.text = id.toString()
                tvShowName.text = nama.toString()
            }
        }
    }

    private fun btnClearClicked() {
        binding.apply {
            btnClear.setOnClickListener {
                // buat editor
                val editor = multiSharedPref.edit()
                // clear data
                editor.clear()
                // save
                editor.apply()
                // replace value dengan empty string
                tvShowId.text = ""
                tvShowName.text = ""
            }
        }
    }
}
